package filehub.communication.message;

import java.io.File;
import java.io.Serializable;

/**
 * A file sending request message that contains information of the sending file.
 * @author Chinthiti Wisetsombat
 *
 */
public class FileSendingRequestMsg implements Serializable {

	/** File to be sent. */
	private File file;
	
	/** 
	 * Constructor for FileSendingRequest.
	 * @param file is the file to be sent.
	 */
	public FileSendingRequestMsg(File file){
		this.file = file;
	}
	
	/**
	 * Return names of files that will be sent.
	 * @return names of files that will be sent.
	 */
	public String getFilename() {
		return file.getName();
	}
	
	public File getFile(){
		return this.file;
	}

}
