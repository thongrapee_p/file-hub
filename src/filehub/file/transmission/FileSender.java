package filehub.file.transmission;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;

import javax.swing.SwingWorker;

import filehub.file.FileDetails;
import filehub.util.FileSelectionUI;

/**
 * Open a server socket and wait for incoming connection to send the selected file.
 * @author Chinthiti Wisetsombat
 * @author Thongrapee Panyapatiphan
 *
 */
public class FileSender extends SwingWorker<Long, Void> {

	private int port = 5050;
	private long byteSent;
	private FileDetails details;
	private String filepath;
	private FileSenderUI ui;
	
	/**
	 * Constructor
	 * @param port is port that will be used in this sending session
	 */
	public FileSender(int port, FileSenderUI ui) {
		this.port = port;
		this.ui = ui;
	}

	/**
	 * Wait for incoming connection, Send file details, 
	 * and write data to the OutputStream.
	 * @return number of bytes sent
	 * @throws IOException
	 */
	public long sendFile() throws IOException {
		
		ServerSocket sendServer = null;
		Socket sendSocket;
		byte data[];
		
		try {
			File file = new File(filepath);
			ui.setFileName(filepath);
			ui.setFileSize(file.length());
			sendServer = new ServerSocket( port );
			sendSocket = sendServer.accept();
			
			// File Object for accesing file Details
			data = new byte[8192]; 
			details = new FileDetails();
			details.setDetails( file.getName(), file.length() );

			// Sending file details to the receiver
			ObjectOutputStream sendDetails = new ObjectOutputStream( sendSocket.getOutputStream() );
			sendDetails.writeObject( details );
			sendDetails.flush();
			
			// Sending File Data 
			FileInputStream fileStream = new FileInputStream( file );
			BufferedInputStream fileBuffer = new BufferedInputStream( fileStream );
			OutputStream out = sendSocket.getOutputStream();
			
		
			// Read Data from InputStream and write to OutputStream
			int count;
			while ( (count = fileBuffer.read(data)) > 0 ) {
				byteSent += count;
				out.write( data, 0, count );
				out.flush();
				this.publish();
			}
			out.close();
			fileBuffer.close();
			fileStream.close();

		} catch (Exception e) {
			e.printStackTrace();
		}

		return byteSent;
	}
	
	@Override
	protected Long doInBackground() throws Exception {
		return sendFile();
	}
	
	/**
	 * Set path of saving file.
	 * @param filepath is the path that file will be saved
	 */
	public void setFile( String filepath ){
		this.filepath = filepath;
	}

	@Override
	protected void done() {
		ui.dispose();
	}

	@Override
	protected void process( List<Void> arg0 ) {
		ui.update( byteSent );
	}
	
}
