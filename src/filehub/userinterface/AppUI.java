package filehub.userinterface;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Desktop;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JSplitPane;
import javax.swing.JTextField;

import filehub.util.FileUiLoader;
import filehub.util.JLabelWithFile;

import javax.swing.JScrollPane;

/**
 * UserInterface of FileSending application, ask for IP Address input of the file destination,
 * choose file, and show received file. It also contains settings menu for choosing the saving directory.
 * @author Chinthiti Wisetsombat
 * @author Thongrapee Panyapatiphan
 *
 */
public class AppUI implements Runnable {

	private static class Holder {
		static final AppUI INSTANCE = new AppUI();
	}

	private JFrame frame;
	private JTextField txtAddress;
	private JButton btnSendFile;
	private JLabel lblReceiverAddress;
	private JMenuItem settingMenuItem;
	private JScrollPane scrollPane;
	private JPanel scrollPanel;
	private JLabel myIPAddress;

	private List<JLabelWithFile> labels;


	/**
	 * Create the application.
	 */
	private AppUI() {
		initialize();
		loadFileUi();
	}

	/** Get instance of this object. */
	public static AppUI getInstance(){
		return Holder.INSTANCE;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame( "File Hub" );
		frame.setBounds( 100, 100, 450, 300 );
		frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );

		JMenuBar menuBar = new JMenuBar();
		frame.setJMenuBar( menuBar );

		JMenu mnMenu = new JMenu( "Menu" );
		menuBar.add( mnMenu );

		settingMenuItem = new JMenuItem( "Setting" );
		mnMenu.add( settingMenuItem );
		frame.getContentPane().setLayout( new BorderLayout(0, 0) );
		

		
		myIPAddress = new JLabel("Your current local IP Address: " + getIPAddress() );
		myIPAddress.setForeground(Color.green);
		
		JPanel detailsPanel = new JPanel();
		detailsPanel.setBackground(Color.black);
		detailsPanel.add( myIPAddress );
		frame.getContentPane().add( detailsPanel, BorderLayout.NORTH );

		settingMenuItem.addActionListener( evt -> new SettingUI().run() );

		JSplitPane splitPane = new JSplitPane();
		splitPane.setEnabled( false );
		splitPane.setPreferredSize( new Dimension(200, 25) );
		splitPane.setOrientation( JSplitPane.VERTICAL_SPLIT );
		frame.getContentPane().add( splitPane, BorderLayout.CENTER );

		JPanel panelTop = new JPanel();
		splitPane.setLeftComponent( panelTop );
		panelTop.setLayout( new FlowLayout( FlowLayout.CENTER, 5, 5 ) );

		lblReceiverAddress = new JLabel( "Receiver address:" );
		panelTop.add( lblReceiverAddress );

		txtAddress = new JTextField();
		panelTop.add( txtAddress );
		txtAddress.setColumns( 15 );

		btnSendFile = new JButton( "Send File" );
		panelTop.add( btnSendFile );

		scrollPane = new JScrollPane();
		splitPane.setRightComponent( scrollPane );

		scrollPanel = new JPanel();
		scrollPane.setViewportView( scrollPanel );
		scrollPanel.setLayout( new BoxLayout( scrollPanel, BoxLayout.Y_AXIS ) );
	}
	
	/** Get the input string on the IP Address input field. */
	public String getAddress(){
		return txtAddress.getText();
	}

	/** Load the view for saved file on the setting directory, also added open file functionality. */
	public void loadFileUi(){
		labels = FileUiLoader.getInstance().getAllLabel();
		for ( JLabelWithFile label : labels ) {
			scrollPanel.add( label );
		}
		addClickToOpen();
	}

	@Override
	public void run(){
		frame.setVisible(true);
	}

	/** Add send button listener to this user interface. */
	public void addSendButtonListener( ActionListener al ){
		btnSendFile.addActionListener( al );
	}

	/** Refresh the view of files on received file directory. */
	public void reloadFileUi() {
		scrollPanel.removeAll();
		loadFileUi();
	}

	/** Provide the view of files on received file directory with click to open functionality. */
	private void addClickToOpen() {
		for ( JLabelWithFile label : labels ) {
			label.addMouseListener( new MouseAdapter(){
				@Override
				public void mouseClicked( MouseEvent arg0 ) {
					try {
						Desktop.getDesktop().open( label.getFile() );
					} catch (IOException e) {
						e.printStackTrace();
					}
				}

			});
		}
	}
	
	/** Get current ip address of the user. */
	private String getIPAddress(){
		String ip = null;
		try {
			ip = InetAddress.getLocalHost().getHostAddress();
		} catch (UnknownHostException e) {
			e.printStackTrace();
		}
		return ip;
	}
	
}
