
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.JOptionPane;

import filehub.communication.Communicator;
import filehub.util.FileSelectionUI;
import filehub.util.SettingLoader;
import filehub.userinterface.*;

/**
 * Controller that controls the userinterface and delegates works to other classes. 
 * @author Thongrapee Panyapatiphan
 * @author Chinthiti Wisetsombat
 *
 */
public class AppController {
	
	private AppUI appUI;
	private Communicator communicator;
	private SettingLoader settingLoader;
	
	/** Constructor, create a communicator for communication between computers, and actionListener to the UI
	 * and initialize settings.
	 */
	public AppController(){
		communicator = new Communicator();
		settingLoader = SettingLoader.getInstance();
		
		appUI = AppUI.getInstance();
		appUI.addSendButtonListener( sendHandler );

	}

	/** Try to connect with the input ip address, then ask for user to choose file to send. */
	ActionListener sendHandler = e -> {
		try {
			String address = appUI.getAddress();
			
			communicator.readyToSendMessageTo( address );

			FileSelectionUI selector = new FileSelectionUI( settingLoader.getSetting( "fileChooserDir" ) );
			File choosen = selector.chooseInputFile();

			if ( choosen != null )
				communicator.sendFileSendingRequestToReceiver( choosen );

		} catch ( IOException ioe ) {
			JOptionPane.showMessageDialog(null, "Invalid IP Address", "Warning", JOptionPane.WARNING_MESSAGE);
		}
	};
	

}
