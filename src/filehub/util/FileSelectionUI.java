package filehub.util;
import java.io.File;

import javax.swing.JFileChooser;
import javax.swing.JFrame;

/**
 * Show a dialog for choosing file when user input a valid ip address and receiver can be connected.
 * @author Chinthiti Wisetsombat
 * @author Thongrapee Panyapatiphan
 *
 */
public class FileSelectionUI extends JFrame {
	
	private SettingLoader settingLoader;
	private String currentDir;
	
	public FileSelectionUI( String currentDir ) {
		this.currentDir = currentDir;
		settingLoader = SettingLoader.getInstance();
	}
	
	/** Shows a file chooser for choosing file. */
	public File chooseInputFile() {
		JFileChooser fc = new JFileChooser( currentDir );
		int result = fc.showOpenDialog( this );
		File dir = fc.getCurrentDirectory();
		settingLoader.changeSetting( "fileChooserDir", dir.getPath().replace('\\','/') );
		settingLoader.writeBundleFile();
		
		if ( result == JFileChooser.APPROVE_OPTION )
			return fc.getSelectedFile();
        else 
        	return null; 
		
	}

}
