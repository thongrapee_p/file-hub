package filehub.util;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.swing.Icon;
import javax.swing.SwingConstants;
import javax.swing.filechooser.FileSystemView;

/**
 * Loads files and labels in the received files directory to show to the user.
 * @author Chinthiti Wisetsombat
 * @author Thongrapee Panyapatiphan
 *
 */
public class FileUiLoader {

	private SettingLoader settingLoader;
	private File[] loadedFiles;
	private static FileUiLoader instance = null;
	
	/** Get instance of this object. using lazy instantiation. */
	public static FileUiLoader getInstance(){
		if ( instance == null ) instance = new FileUiLoader();
		return instance;
	}
	
	private FileUiLoader(){
		settingLoader = SettingLoader.getInstance();
	}
	
	/** Load received file in the directory. */
	private void loadFilesInSaveDir(){
		String dir$ = settingLoader.getSetting( "receivedFileDir" );

		File dir = new File( dir$ );
		
		loadedFiles = dir.listFiles();
		
	}
	
	/** Get the label of a single file.*/
	public JLabelWithFile getLabel( File file ){
		Icon icon =  FileSystemView.getFileSystemView().getSystemIcon( file );
		
		return new JLabelWithFile( file.getName(), icon, SwingConstants.LEFT, file );
	}
	
	/** Get all labels of file in the received file directory. */
	public List<JLabelWithFile> getAllLabel(){
		loadFilesInSaveDir();
		ArrayList<JLabelWithFile> temp = new ArrayList<JLabelWithFile>();
		for ( File file : loadedFiles ) {
			temp.add( getLabel(file) );
		}
		return temp;
	}
}
