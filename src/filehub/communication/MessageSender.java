package filehub.communication;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import com.lloseng.ocsf.client.AbstractClient;

import filehub.communication.message.FileSendingRequestRespondMsg;
import filehub.communication.message.ReadyToSendMsg;
import filehub.file.transmission.FileSender;
import filehub.file.transmission.FileSenderUI;

/**
 * Send file sending request to a specific address with port 5000
 * if user agree to receive file, prepare a file sender with open port 5100
 * then send another message to the message receiver containing address and port of waiting file.
 * 
 * if user decline, then show a message.
 *
 * @author Chinthiti Wisetsombat
 * @author Thongrapee Panyapatiphan
 *
 */
public class MessageSender extends AbstractClient{

	public static final int COMMUNICATION_PORT = 5000;
	private static int fileSendingPort = 5100;
	public MessageSender(String host) {
		super(host, COMMUNICATION_PORT);
	}
	

	@Override
	protected void handleMessageFromServer(Object msg) {
		if ( msg instanceof FileSendingRequestRespondMsg ) { 
			FileSendingRequestRespondMsg respond = (FileSendingRequestRespondMsg) msg;
			if ( respond.isRequestAccepted() ) {
				// find available port on this sender
				boolean portIsAvailable = tryToOpenServerSocket( fileSendingPort );
				while ( !portIsAvailable ) {
					portIsAvailable = tryToOpenServerSocket( ++fileSendingPort );
					System.out.println( "trying to find port" );
				}
				//prepare to send files.
				FileSenderUI ui = new FileSenderUI( super.getHost(), fileSendingPort );
				FileSender sender = new FileSender( fileSendingPort, ui );
				
				SwingUtilities.invokeLater( ui );
				sender.setFile( respond.getFile().getPath() );
				sender.execute();	
				
				try {
					this.sendToServer( new ReadyToSendMsg( fileSendingPort, InetAddress.getLocalHost().getHostAddress() ) );
				} catch (IOException e) {
					e.printStackTrace();
				}
				fileSendingPort++;
			
			} else {
				JOptionPane.showMessageDialog( null, "Request is decline", null, JOptionPane.INFORMATION_MESSAGE) ;
			}
		}
	}
	
	private boolean tryToOpenServerSocket(int port){
		ServerSocket servSocket = null;
		try{
			servSocket = new ServerSocket(port);
		} catch (IOException ioe){
			
		} finally {
			if(servSocket != null)
				try {
					servSocket.close();	
					return true;
				} catch (final IOException e) { /*do nothing.*/ }
			return false;
		}
	}
	
	
}
