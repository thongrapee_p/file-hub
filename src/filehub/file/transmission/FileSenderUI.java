package filehub.file.transmission;

import javax.swing.JFrame;
import javax.swing.BoxLayout;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import java.awt.FlowLayout;

/**
 * View of the file sender, showing the current status of transfering file, including name, and size.
 * @author Chinthiti Wisetsombat
 * @author Thongrapee Panyapatiphan
 *
 */
public class FileSenderUI implements Runnable{

	private JFrame frame;
	private String host;
	private int port;
	private JProgressBar progressBar;
	private String filename;
	private long filesize;
	private JLabel lblFilename;

	/**
	 * Create the application.
	 */
	public FileSenderUI(String host, int port) {
		this.host = host;
		this.port = port;
		initialize();
		frame.setTitle( "Sending to " + host );
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds( 100, 100, 500, 110 );
		frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		frame.getContentPane().setLayout( new BoxLayout(frame.getContentPane(), BoxLayout.Y_AXIS) );
		
		JPanel panelInformation = new JPanel();
		frame.getContentPane().add( panelInformation );
		panelInformation.setLayout( new FlowLayout(FlowLayout.LEFT, 5, 5) );
		
		JLabel lblFile = new JLabel( "File :" );
		panelInformation.add( lblFile );
		
		lblFilename = new JLabel( "filename" );
		panelInformation.add( lblFilename );
		
		JPanel panel = new JPanel();
		frame.getContentPane().add( panel );
		panel.setLayout( new FlowLayout(FlowLayout.LEFT, 5, 5) );
		
		JLabel lblSendTo = new JLabel( "Send To: " );
		panel.add( lblSendTo );
		
		JLabel lblAddress = new JLabel( host + " on port " + port );
		panel.add( lblAddress );
		
		progressBar = new JProgressBar();
		progressBar.setMaximum( 10000 );
		progressBar.setStringPainted( true );
		frame.getContentPane().add( progressBar );
	}

	@Override
	public void run(){
		frame.setVisible( true );
	}
	
	public void dispose(){
		frame.dispose();
	}
	
	/**
	 * Update the progress bar to show current sending progress. 
	 * @param byteSent
	 */
	public void update( long byteSent ){
		progressBar.setValue( (int)( byteSent*10000 / filesize ) );
		progressBar.setString( byteSent + " / " + filesize + "bytes" );
	}
	
	/**
	 * Set the file size label.
	 * @param filesize is the size of the file
	 */
	public void setFileSize( long filesize ){
		this.filesize = filesize;
	}
	
	/**
	 * Set the file name label.
	 * @param filename is the name of the file
	 */
	public void setFileName( String filename ){
		this.filename = filename;
		lblFilename.setText( filename );
		
	}
}
