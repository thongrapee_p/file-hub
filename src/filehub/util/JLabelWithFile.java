package filehub.util;

import java.io.File;

import javax.swing.Icon;
import javax.swing.JLabel;

/**
 * Create JLabel containing file.
 * @author Chinthiti Wisetsombat
 * @author Thongrapee Panyapatiphan
 *
 */
public class JLabelWithFile extends JLabel {

	private File file;
	
	public JLabelWithFile(String text, Icon icon, int horizontalAlignment, File file){
		super( text, icon, horizontalAlignment );
		this.file = file;
	}
	
	public File getFile(){
		return this.file;
	}
}
