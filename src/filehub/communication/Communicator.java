package filehub.communication;

import java.io.File;
import java.io.IOException;

import filehub.communication.message.FileSendingRequestMsg;

/**
 * Communicator is class for communication of the program.
 * @author Chinthiti Wisetsombat.
 *
 */
public class Communicator {

	/** Message Receiver for receiving messages. */
	private MessageReceiver msgReceiver;
	/** MessageSender for sending messages. */
	private MessageSender msgSender;
	
	
	/**
	 * Constructor of Communicator
	 */
	public Communicator(){
		msgReceiver = new MessageReceiver();
		try {
			msgReceiver.listen();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Prepare the Communicator to send messages to other Communicator.
	 * @param host is the communicator server host name.
	 * @throws IOException if host is not available.
	 */
	public void readyToSendMessageTo(String host) throws IOException {
		msgSender = new MessageSender(host);
		msgSender.openConnection();
	}
	
	/**
	 * Send File Sending Request to other Communicator.
	 * @param host is the cummunicator server host name.
	 * @throws IOException if connection of given host has not opened yet.
	 */
	public void sendFileSendingRequestToReceiver(File file) throws IOException {
		FileSendingRequestMsg fsr = new FileSendingRequestMsg(file);
		msgSender.sendToServer(fsr);
	}
	
	
}
