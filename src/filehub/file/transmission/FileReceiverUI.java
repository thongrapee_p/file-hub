package filehub.file.transmission;

import java.awt.Color;
import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;

import filehub.util.SettingLoader;

/**
 * A view of FileReceiver showing the current progress of receiving file, 
 * shows the filesender ip, saving directory,
 * file size, and name of the file.
 * @author Chinthiti Wisetsombat
 * @author Thongrapee Panyapatiphan
 *
 */
public class FileReceiverUI extends JFrame implements Runnable {

	private JPanel contentPane;
	private long filesize;
	private JProgressBar progressBar;
	private JLabel lblFilename;
	private String toReceive;

	private String host;
	private int port;
	private FileReceiver receiver;
	private JPanel panel;
	private SettingLoader settingLoader;
	private JLabel lblSaveDir;
	
	public FileReceiverUI(String host, int port) {
		this.host = host;
		this.port = port;
		initComponents();
		receiver = new FileReceiver( this );
		receiver.setDestination( host, port );
		settingLoader = SettingLoader.getInstance();
		
	}
	
	public void initComponents(){
		setResizable( false );
		setBackground( Color.LIGHT_GRAY );
		setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE );
		
		setBounds( 150, 150, 500, 140 );
		
		contentPane = new JPanel();
		contentPane.setBorder( new EmptyBorder(5, 5, 5, 5) );
		setContentPane( contentPane );
		contentPane.setLayout( new BoxLayout(contentPane, BoxLayout.Y_AXIS) );
		
		panel = new JPanel();
		contentPane.add(panel);
		panel.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));
		
		JLabel lblFile = new JLabel( "File:" );
		panel.add( lblFile );
		
		lblFilename = new JLabel( "filename" );
		panel.add( lblFilename );
		
		JPanel panel_1 = new JPanel();
		contentPane.add( panel_1 );
		panel_1.setLayout( new FlowLayout(FlowLayout.LEFT, 5, 5) );
		
		JLabel lblSaveTo = new JLabel( "Save to:" );
		panel_1.add( lblSaveTo );
		
		lblSaveDir = new JLabel();
		panel_1.add( lblSaveDir );
		
		JPanel panel_information = new JPanel();
		contentPane.add( panel_information );
		panel_information.setLayout( new FlowLayout(FlowLayout.LEFT, 5, 5) );
		
		JLabel lblFrom = new JLabel( "From:" );
		panel_information.add( lblFrom );
		
		JLabel lblHostname = new JLabel( host + " on Port " + port );
		panel_information.add( lblHostname );
		
		progressBar = new JProgressBar( SwingConstants.HORIZONTAL );
		progressBar.setMaximum( 10000 );
		contentPane.add( progressBar );
		
		progressBar.setStringPainted( true );
		progressBar.setString( " 0 / 0 bytes" );
		
	}
	
	@Override
	public void run() {
		this.setVisible( true );
		this.setTitle( "Receiving from " + host );
		lblSaveDir.setText( getDirectory() );
		receiver.execute();
		
	}
	
	public void update(long downloadedSize) {
		int progress = (int) ( downloadedSize*10000/filesize );
		progressBar.setValue( progress );
		progressBar.setString( downloadedSize + toReceive );
	}
	
	public void setFilesize(long filesize) {
		this.filesize = filesize;
		toReceive = " / " + filesize + " bytes";
		progressBar.setString( " 0" + toReceive );
	}
	
	public void setFilename(String filename) {
		lblFilename.setText( filename );
	}
	
	public String getDirectory() {
		return settingLoader.getSetting( "receivedFileDir" );
	}

}
