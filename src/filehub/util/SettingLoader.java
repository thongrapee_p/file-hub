package filehub.util;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;

/**
 * Load setting from a ResourceBundle, the settings contains directory of received file.
 * @author Chinthiti Wisetsombat
 * @author Thongrapee Panyapatiphan
 *
 */
public class SettingLoader {

	private ResourceBundle bundle;
	private String userHome;
	private static String bundleName = "/settings.properties";
	private static String[] dirName = new String[] {"/File-hub", "/Received Files"};
	private HashMap<String,String> settings;
	
	private static SettingLoader instance = new SettingLoader();
	
	private SettingLoader(){
		userHome = System.getProperty( "user.home" );
		userHome = userHome.replace( "\\", "/" );
		
		checkAndCreateDirectory();
		
		if ( !isBundleAvailable() ) {
			settings = createDefaultSettings();
			writeBundleFile();
		} else {
			loadBundle();
			settings = loadSettings();
		}
		
	}
	
	public boolean isBundleAvailable(){
		File bundle = new File(userHome + dirName[0] + bundleName);		
		return bundle.exists();
	}
	
	public void checkAndCreateDirectory(){
		File dir = new File( userHome + dirName[0] );
		
		if ( !dir.exists() ) dir.mkdir();
			
		File downloadDir = new File(dir.toString() + dirName[1]);
		
		if ( !downloadDir.exists() ) downloadDir.mkdir();
			
		
	}
	
	public void writeBundleFile() {
		File bundle = new File( userHome+ "/" + dirName[0] + "/" + bundleName );
		if ( bundle.exists() ) { 
			
			bundle.delete();
			try {
				bundle.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try{
			
			FileOutputStream fos = new FileOutputStream( bundle );
			PrintWriter pw = new PrintWriter( fos, true) ;

			Set<String> keys = settings.keySet();
			for ( String key : keys ) {
				String value = settings.get( key );
				pw.println( key + " = " + value );
			}
			
			pw.close();
			fos.close();
		} catch (IOException e){
			//do nothing.
		}

		if ( System.getProperty("os.name").startsWith("Windows") ) {
			try {
				Runtime.getRuntime().exec( "attrib +h " + bundle.getPath() );
			} catch (IOException e) {
				//do nothing
			}
		}
		
		
	}
	
	public void loadBundle(){
		File dir = new File( userHome + dirName[0] );
		URL[] urls = null;
		try { 
			urls = new URL[] {dir.toURI().toURL()};
			ClassLoader loader = new URLClassLoader( urls );
			bundle = ResourceBundle.getBundle( "settings", Locale.getDefault(), loader );
		} catch (MalformedURLException e) {
			//do nothing
		}
		
	}
	
	public String getSetting(String key){
		return settings.get(key);
	}
	
	public HashMap<String,String> loadSettings(){
		Set<String> keys = bundle.keySet();
		Iterator<String> it = keys.iterator();
		HashMap<String, String> temp = new HashMap<String, String>();
		while ( it.hasNext() ) {
			String key = it.next();
			String value = bundle.getString(key);
			temp.put(key, value);
		}
		return temp;
		
	}
	
	public void changeSetting(String key, String value){
		settings.put( key, value );
		writeBundleFile();
	}
	
	public HashMap<String,String> createDefaultSettings(){
		String[] keys = {"receivedFileDir", "fileChooserDir"};
		String[] values = { userHome + dirName[0] + dirName[1], userHome };
		
		HashMap<String,String> temp = new HashMap<String,String>(); 
		for(int i = 0; i < keys.length; i++) { temp.put(keys[i], values[i]); }
		
		return temp;
	}
	
	public static SettingLoader getInstance(){
		return instance;
	}
}
