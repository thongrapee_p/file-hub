
package filehub.userinterface;

import java.awt.BorderLayout;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionListener;
import java.io.File;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import filehub.util.SettingLoader;

/**
 * A menu for choosing directory to save received files.
 * @author Chinthiti Wisetsombat
 * @author Thongrapee Panyapatiphan
 *
 */
public class SettingUI implements Runnable {

	private JFrame frame;
	private JTextField textField;
	private SettingLoader settingLoader;
	private JButton btnBrowse;
	private JButton btnSave;
	private JButton btnCancel;
	private AppUI appUI;

	/**
	 * Create the application.
	 */
	public SettingUI() {
		settingLoader = SettingLoader.getInstance();
		this.appUI = AppUI.getInstance();
		initialize();
		addAction();
		frame.pack();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame( "Settings" );
		frame.setBounds( 100, 100, 450, 300 );
		frame.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE) ;
		frame.getContentPane().setLayout( new BorderLayout(0, 0) );

		JPanel panel = new JPanel();
		frame.getContentPane().add( panel );
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 100, 0, 0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 0.0, 1.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout( gbl_panel );

		JLabel lblSaveTo = new JLabel( " Save To:" );
		GridBagConstraints gbc_lblSaveTo = new GridBagConstraints();
		gbc_lblSaveTo.anchor = GridBagConstraints.EAST;
		gbc_lblSaveTo.insets = new Insets( 0, 0, 5, 5 );
		gbc_lblSaveTo.gridx = 1;
		gbc_lblSaveTo.gridy = 1;
		panel.add( lblSaveTo, gbc_lblSaveTo );

		textField = new JTextField( settingLoader.getSetting( "receivedFileDir" ) );
		textField.setEditable( false );
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.gridwidth = 2;
		gbc_textField.insets = new Insets( 0, 0, 5, 5 );
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 2;
		gbc_textField.gridy = 1;
		panel.add( textField, gbc_textField );
		textField.setColumns( 20 );

		btnBrowse = new JButton( "Browse..." );
		GridBagConstraints gbc_btnBrowse = new GridBagConstraints();
		gbc_btnBrowse.insets = new Insets( 0, 0, 5, 5 );
		gbc_btnBrowse.gridx = 4;
		gbc_btnBrowse.gridy = 1;
		panel.add( btnBrowse, gbc_btnBrowse );

		btnSave = new JButton( "Save" );
		GridBagConstraints gbc_btnSave = new GridBagConstraints();
		gbc_btnSave.insets = new Insets( 0, 0, 0, 5 );
		gbc_btnSave.gridx = 3;
		gbc_btnSave.gridy = 4;
		panel.add( btnSave, gbc_btnSave );

		btnCancel = new JButton( "Cancel" );
		GridBagConstraints gbc_btnCancel = new GridBagConstraints();
		gbc_btnCancel.insets = new Insets( 0, 0, 0, 5 );
		gbc_btnCancel.gridx = 4;
		gbc_btnCancel.gridy = 4;
		panel.add( btnCancel, gbc_btnCancel );

	}

	@Override
	public void run(){
		frame.setVisible( true );
	}

	private void addAction(){
		ActionListener browse = evt -> {
			JFileChooser chooser = new JFileChooser( settingLoader.getSetting("receivedFileDir") );
			chooser.setFileSelectionMode( JFileChooser.DIRECTORIES_ONLY );

			int result = chooser.showOpenDialog( frame );
			if ( result == JFileChooser.APPROVE_OPTION ) {
				File file = chooser.getSelectedFile();
				textField.setText( file.getPath().replace('\\', '/') );
			}
		};

		btnBrowse.addActionListener( browse );
		btnCancel.addActionListener( evt -> frame.dispose() );

		ActionListener save = evt -> {
			String path = textField.getText();
			path = path.replace( '\\', '/' );
			settingLoader.changeSetting( "receivedFileDir", path );
			appUI.reloadFileUi();
			frame.dispose();
			
		};

		btnSave.addActionListener( save );
	}


}
