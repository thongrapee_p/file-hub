package filehub.file;
import java.io.Serializable;

/**
 * Collects details of a file including name and size.
 * 
 * @author Chinthiti Wisetsombat
 * @author Thongrapee Panyapatiphan
 *
 */
public class FileDetails implements Serializable {

	String name;
	long size;

	/** Set the details of file. */
	public void setDetails(String name, long size) {
		this.name = name;
		this.size = size;
	}

	/** Get name of the file. */
	public String getName() {
		return name;
	}

	/** Get size of the file. */
	public long getSize() {
		return size;
	}
}