package filehub.file.transmission;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.List;

import javax.swing.SwingWorker;

import filehub.file.FileDetails;
import filehub.userinterface.AppUI;

/**
 * Connect to a server socket, get input stream, and write received data from
 * the stream to file.
 * 
 * @author Chinthiti Wisetsombat
 * @author Thongrapee Panyapatiphan
 *
 */
public class FileReceiver extends SwingWorker<Long, Void> { 

	public String host;
	public int port;

	private FileDetails details;
	private long bytesRead;
	private FileReceiverUI ui;

	/**
	 * Constructor
	 * @param ui is the userinterface
	 */
	public FileReceiver(FileReceiverUI ui) {
		this.ui = ui;
	}

	/**
	 * Set the host name and port, necessary for receiving input from a socket.
	 * 
	 * @param host is the host name or ip address of the sender
	 * @param port is port of the sender
	 */
	public void setDestination(String host, int port) {
		this.host = host;
		this.port = port;
		bytesRead = 0L;
	}

	/**
	 * Connect to a server socket with designated host and port, get inputstream.
	 * and write data into a file.
	 * 
	 * @return number of bytes read
	 * @throws IOException
	 */
	public long downloadFile() throws IOException {

		try {
			
			Socket receiveSocket = new Socket( host, port );

			ObjectInputStream getDetails = new ObjectInputStream( receiveSocket.getInputStream() );
			details = (FileDetails) getDetails.readObject();

			String fileName = details.getName();
			ui.setFilename( fileName );
			ui.setFilesize( details.getSize() );
			
			byte data[] = new byte[ 8192 ];
			File temp = new File( ui.getDirectory() + "/" + fileName );
			FileOutputStream fileOut = new FileOutputStream( temp );
			InputStream fileIn = receiveSocket.getInputStream();
			BufferedOutputStream fileBuffer = new BufferedOutputStream( fileOut );
			
			int count;
			while ( ( count = fileIn.read(data) ) > 0 ) {
				bytesRead += count;
				fileBuffer.write( data, 0, count );
				fileBuffer.flush();
				this.publish();
			}

			fileBuffer.close();
			fileIn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return bytesRead;
	}

	@Override
	protected Long doInBackground() throws Exception {
		return downloadFile();
	}

	@Override
	protected void done() {
		ui.update(bytesRead);
		ui.dispose();
		AppUI.getInstance().reloadFileUi();
	}

	@Override
	protected void process(List<Void> chunks) {
		ui.update(bytesRead);
	}

	/** Get the number of byte read so far. */
	public long getBytesRead() {
		return bytesRead;
	}

}