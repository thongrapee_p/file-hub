
import javax.swing.SwingUtilities;

import filehub.userinterface.AppUI;

/**
 * Main class of the application that runs the program. Initialize a controller and show the userinterface.
 * @author Thongrapee Panyapatiphan
 * @author Chinthiti Wisetsombat
 *
 */
public class Main {

	/** Create a controller to set things up, and runs the user interface. */
	public static void main(String[]args){
		AppController controller = new AppController();
		SwingUtilities.invokeLater( AppUI.getInstance() );
	}
}
