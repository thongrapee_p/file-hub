package filehub.communication;

import java.io.IOException;


import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import com.lloseng.ocsf.server.AbstractServer;
import com.lloseng.ocsf.server.ConnectionToClient;

import filehub.communication.message.FileSendingRequestMsg;
import filehub.communication.message.FileSendingRequestRespondMsg;
import filehub.communication.message.ReadyToSendMsg;
import filehub.file.transmission.FileReceiverUI;

/**
 * Receive file sending request and ask the user if he/she wanted to receive to file,
 * if user agree to receive file, send confirmation message to sender to begin sending file.
 * otherwise, decline the request.
 * 
 * When user agrees to receive file from the sender, 
 * the sender will send the address and opened port with
 * file waiting for connection.
 * 
 * The host address and port will be used by a filereceiver to receive the file.
 * @author Chinthiti Wisetsombat
 * @author Thongrapee Panyapatiphan
 *
 */
public class MessageReceiver extends AbstractServer {

	
	public static final int COMMUNICATION_PORT = 5000;

	public MessageReceiver() {
		super(COMMUNICATION_PORT);
	}

	@Override
	protected void handleMessageFromClient(Object msg, ConnectionToClient client) {
		String[] options = new String[] {"Yes", "No"};
		int option = -1;
		if( msg instanceof FileSendingRequestMsg ){
			FileSendingRequestMsg request = (FileSendingRequestMsg) msg;
			// Create JOptionPane to ask user
			option = JOptionPane.showOptionDialog( null,
					"Do you want to receive" + request.getFilename() +  " from " + client.getInetAddress().getHostAddress(),
					"Incoming File Sending Request",
					JOptionPane.YES_NO_OPTION,
					JOptionPane.QUESTION_MESSAGE,
					null,
					options,
					options[1]);
			
			
			// send respond to sender.
			try {
				if ( option == 0 ) {
					client.sendToClient( new FileSendingRequestRespondMsg( request.getFile() ) );
				} else {
					client.sendToClient( new FileSendingRequestRespondMsg( false ) );
				}
			} catch ( IOException ioe ) {
				System.err.println( ioe.getMessage() );
			}
		}
		
		else if ( msg instanceof ReadyToSendMsg ) {
			ReadyToSendMsg message = (ReadyToSendMsg) msg;
			FileReceiverUI receiverUI = new FileReceiverUI( message.getHost(), message.getPort() );
			SwingUtilities.invokeLater( receiverUI );
		}
			
	}

}
