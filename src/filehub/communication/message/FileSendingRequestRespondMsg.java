package filehub.communication.message;

import java.io.File;
import java.io.Serializable;

/**
 * PortOpeningRequestRespondMessage is the responder message which respond to the PortOpeningRequestMessage.
 * @author Chinthiti Wisetsombat
 *
 */
public class FileSendingRequestRespondMsg implements Serializable {

	/** Is port opening request accepted. */
	private boolean requestAccepted;
	/** File to be received. */
	private File file;
	
	/**
	 * A constructor of PortOpeningRequestRespondMessage which only accepted or decline the request.
	 * @param requestAccepted is true is PortOpeningRequest is accepted, otherwise false.
	 */
	public FileSendingRequestRespondMsg(boolean requestAccepted) {
		this.requestAccepted = requestAccepted;
		file = null;
	}
	
	public FileSendingRequestRespondMsg(File file){
		this.requestAccepted = true;
		this.file = file;
	}
	
	/**
	 * Return whether request is accepted or not.
	 * @return true if request is accpeted, otherwise return false.
	 */
	public boolean isRequestAccepted() {
		return requestAccepted;
	}

	public File getFile(){
		return this.file;
	}
}
