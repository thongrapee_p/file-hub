package filehub.communication.message;

import java.io.Serializable;

/**
 * A serializable message containing host address and port.
 * @author Chinthiti Wisetsombat
 * @autohr Thongrapee Panyapatiphan
 *
 */
public class ReadyToSendMsg implements Serializable {

	private int port;
	private String host;
	
	public ReadyToSendMsg(int port, String host){
		this.port = port;
		this.host = host;
	}
	
	public int getPort(){
		return port;
	}
	
	public String getHost(){
		return host;
	}
	
}
